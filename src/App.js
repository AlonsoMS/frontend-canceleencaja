import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import InfoPanel from './ventas/layout/infoPanel.layout'

function App() {
  return (
    <div className="App">
      <InfoPanel/>
    </div>
  );
}

export default App;
