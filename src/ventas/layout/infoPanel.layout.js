import SpinnerLoader from '../components/spinner.component'
import { useState } from 'react'
import ProductsList from '../components/productsList.component'


function InfoPanel (props) {
    const [loaded, setLoadedState] = useState(false)
    
    return (
        <div>
            <SpinnerLoader dataLoaded={loaded}/>
            <ProductsList setLoadedState={setLoadedState} loaded={loaded}/>
        </div>
    )
}

export default InfoPanel