import { useState, useEffect} from 'react'
import axios from 'axios'
import { Container ,Card, Form } from 'react-bootstrap'

function ProductsList (props) {
    const [productsData, setProductsData] = useState([])
    useEffect(() => {
        const fetchData = async () => {
            if (!props.loaded) {
                const result = await axios.get('http://192.168.43.22:3001/inventario')
                console.log(result.data)
                if (result.data.status === "OK") { 
                    props.setLoadedState(true)
                    setProductsData(result.data.productos)
                }
            }
        }

        fetchData()
    })
    return (
        <div>
            <Container>
            <Form.Control type="email" placeholder="¿Que desea buscar?"/>
            </Container>
            <Container>
                    
                    {productsData.map((product, index) => 
                        <Card key={product.codigoBarra}>
                        <Card.Body>
                            <Card.Title>{ product.name}</Card.Title>
                            <Card.Text>
                                {product.precio}
                            </Card.Text>
                        </Card.Body>
                        </Card>
                     )}
            </Container>
        </div>
    )
}

export default ProductsList

